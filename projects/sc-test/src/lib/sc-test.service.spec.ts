import { TestBed } from '@angular/core/testing';

import { ScTestService } from './sc-test.service';

describe('ScTestService', () => {
  let service: ScTestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScTestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
