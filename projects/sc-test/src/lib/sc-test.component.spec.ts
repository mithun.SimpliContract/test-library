import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScTestComponent } from './sc-test.component';

describe('ScTestComponent', () => {
  let component: ScTestComponent;
  let fixture: ComponentFixture<ScTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
