import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-sc-test',
  template: `
    <p>
      sc-test works!
    </p>
  `,
  styles: [
  ]
})
export class ScTestComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
