import { NgModule } from '@angular/core';
import { ScTestComponent } from './sc-test.component';



@NgModule({
  declarations: [
    ScTestComponent
  ],
  imports: [
  ],
  exports: [
    ScTestComponent
  ]
})
export class ScTestModule { }
