/*
 * Public API Surface of sc-test
 */

export * from './lib/sc-test.service';
export * from './lib/sc-test.component';
export * from './lib/sc-test.module';
